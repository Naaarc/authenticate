import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { AppMainComponent } from './components/app-main/app-main.component';
import { AuthGuard } from './app-auth/auth.guard';
import { RegisterComponent } from './components/register/register.component';
import { Role } from './models/Role/Role';
import { UserEditComponent } from './components/user/user-edit/user-edit.component';


const routes: Routes = [
    {
        path: 'admin',
        component: AppMainComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.admin, Role.user] },
        children: [
            {
                path: 'user',
                component: UserComponent,
            },
            {
                path: 'user-edit/:id',
                component: UserEditComponent
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    // otherwise redirect to home
    { path: '**', redirectTo: 'login' }];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
