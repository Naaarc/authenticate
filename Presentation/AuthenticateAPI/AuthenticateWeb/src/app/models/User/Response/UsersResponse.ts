import { User } from '../User';

export class UsersResponse {

    constructor() { };

    users: Array<User>;
}
