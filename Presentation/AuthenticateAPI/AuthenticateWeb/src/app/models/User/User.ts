
import { RoleModel } from '../Role/RoleModel';

export class User {
    constructor() { };

    userId: number;
    email: string;
    name: string;
    lastName: string;
    password: string;
    userRole: RoleModel;
}
