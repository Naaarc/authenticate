export class AuthenticateModel {
    constructor() { };

    name: string;
    roleName: string;
    token: string;
}
