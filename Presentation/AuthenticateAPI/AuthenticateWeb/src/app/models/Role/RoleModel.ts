export class RoleModel {
    constructor() { };

    roleId: number;
    name: string;
    isAssigned: boolean;
}
