
import { RoleModel } from '../RoleModel';

export class RolesResponse {

    constructor() { };

    roles: Array<RoleModel>;
}
