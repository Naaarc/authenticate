import { Component } from '@angular/core';
import { User } from './models/User/User';
import { Router } from '@angular/router';
import { AuthenticationService } from './app-auth/authentication.service';
import { Role } from './models/Role/Role';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    //currentUser: User;

    //constructor(
    //    private router: Router,
    //    private authenticationService: AuthenticationService
    //) {
    //    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    //}

    //get isAdmin() {
    //    return this.currentUser && this.currentUser.userRoleName === Role.name;
    //}

    //logout() {
    //    this.authenticationService.logout();
    //    this.router.navigate(['/login']);
    //}
}
