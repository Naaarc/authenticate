
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';
import { BaseResponse } from '../models/BaseResponse';
import { environment } from '../../environments/environment';
import { RolesResponse } from '../models/Role/Response/RolesResponse';

@Injectable({
    providedIn: 'root'
})

export class RoleService extends BaseService {
    private headers: HttpHeaders;
    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
        
    }
    async getAllRoles(): Promise<BaseResponse<RolesResponse>> {
        const response = await this.baseGetRequest<BaseResponse<RolesResponse>>(environment.baseUrl + '/api/Role/GetRoles', "", this.headers);
        console.log(response,'dsaasdsadsad');
        return response;
    }

}
