import { extend } from 'webdriver-js-extender';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './BaseService';
import { BaseResponse } from '../models/BaseResponse';
import { UsersResponse } from '../models/User/Response/UsersResponse';
import { BooleanResponse } from '../models/BooleanResponse';
import { User } from '../models/User/User';
import { UserResponse } from '../models/User/Response/UserResponse';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class UserService extends BaseService {
    private headers: HttpHeaders;
    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
        //this.headers = this.headers.set('Authorization', `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfb…yMDh9.4NraUtLiEMeM9wd67mRaa3Xu0nnBix6fnL4821d7eGY`);
    }
    async getAllUsers(): Promise<BaseResponse<UsersResponse>> {
        const response = await this.baseGetRequest<BaseResponse<UsersResponse>>(environment.baseUrl +'/api/User/GetUsers', "", this.headers);
        return response;
    }

    async getUser(userId: number): Promise<BaseResponse<UserResponse>> {
        const param = "?UserId=" + userId;
        const response = await this.baseGetRequest<BaseResponse<UserResponse>>(environment.baseUrl + '/api/User/GetUser', param, this.headers);
        return response;
    }

    async createUser(user: User): Promise<BaseResponse<BooleanResponse>> {
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>(environment.baseUrl + '/api/User/CreateUser', JSON.stringify(user), this.headers)
        return null;
    }

    async RemoveUser(userId: number): Promise<BaseResponse<BooleanResponse>> {
        const parm = "?UserId=" + userId;
        console.log(userId);
        const response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>(environment.baseUrl + '/api/User/RemoveUser', parm, this.headers);
        return response;
    }

    async UpdateUser(user: User): Promise<BaseResponse<UserResponse>> {
        console.log(user, 'serwis')
        const response = await this.basePutRequest<BaseResponse<UserResponse>>(environment.baseUrl + '/api/User/UpdateUser', JSON.stringify(user), this.headers);
        return response;
    }

}
