import { Component, OnInit, ViewChild } from '@angular/core';
import {  UserService } from '../../services/userService';
import { Router } from '@angular/router';
import { User } from '../../models/User/User';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { UsersResponse } from '../../models/User/Response/UsersResponse';
import { UserEditComponent } from './user-edit/user-edit.component';
import { AuthenticationService } from '../../app-auth/authentication.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    displayedColumns: string[] = ['email', 'name', 'action'];
    usersList: MatTableDataSource<User>;
    users: Array<User> = [];
    durationInSeconds = 5;

    currentUserLogged: any;
    user: User;

    constructor(private usersService: UserService, private _router: Router, private _snackBar: MatSnackBar, private authenticationService: AuthenticationService, private matDialog: MatDialog) { }

    ngOnInit() {
        this.users = new Array<User>();
        this.usersList = new MatTableDataSource<User>();
        this.getUserList();
        this.currentUserLogged = this.authenticationService.currentUserValue;
        this.user = new User();
    }

    private async getUserList() {
        const userResponse = await this.usersService.getAllUsers();
        this.users = userResponse.data.users;
        this.usersList = new MatTableDataSource<User>(this.users);
        this.usersList.paginator = this.paginator;
    }

    userDeleteButton(userId: number) {
        this.usersService.RemoveUser(userId);
        if (this.currentUserLogged.roleName == "User") {
            this._snackBar.open('You have got no authorization ', 'Sorry', {
                duration: 3000
            });
        }
        else {
            this._snackBar.open('User was deleted', 'Great!', {
                duration: 3000
            });
        }
    }

    assignRoleButton(userId: number) {
        if (this.currentUserLogged.roleName == "Administrator") {
            this.matDialog.open(UserEditComponent, {
                data: { userId: userId }
            })
        }
        else {
            this._snackBar.open('You have got no authorization ', 'Sorry', {
                duration: 3000
            });
        }
    }
}
