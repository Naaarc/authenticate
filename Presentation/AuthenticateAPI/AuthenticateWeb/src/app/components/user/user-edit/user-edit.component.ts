import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserComponent } from '../../../user/user.component';
import { UserService } from '../../../services/userService';
import { User } from '../../../models/User/User';
import { RoleModel } from '../../../models/Role/RoleModel';
import { RoleService } from '../../../services/RoleService';
import { isNullOrUndefined } from 'util';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
    //users
    userId: number;
    user: User;

    //roles
    roles: Array<RoleModel> = [];
    constructor(@Inject(MAT_DIALOG_DATA) public data: any, private matDialogRef: MatDialogRef<UserComponent>, private userService: UserService, private roleService: RoleService) {
        if (data != null) {
            this.userId = data.userId;
        }
    }

    ngOnInit() {
        this.user = new User();
        this.roles = new Array<RoleModel>();
        this.getUser();
    }

    private async getUser() {
        const userResponse = await this.userService.getUser(this.userId);
        this.user = userResponse.data.user;
        this.getRoles(); 
    }

    private async getRoles() {
        const roleResponse = await this.roleService.getAllRoles();
        this.roles = roleResponse.data.roles;
        this.isAssign();
    }

    private async AssignAndUpdateUser() {
        await this.userService.UpdateUser(this.user);
    }

    //Checking which roles is assigned
    isAssign() {
        for (let i of this.roles) {
            if (this.user.userRole != null) {
                if (i.roleId == this.user.userRole.roleId) {
                    i.isAssigned = true;
                }
            }
        }
    }
    //If radiobutton clicked - Assign this role to user
    Assign(roleId: any) {
        for (let i of this.roles) {
            if (roleId == i.roleId) {
                this.user.userRole = { roleId: roleId, name: i.name, isAssigned: true }
            }
        }
    }
    onSaveButton(roleId: number) {
        this.AssignAndUpdateUser();
    }
}
