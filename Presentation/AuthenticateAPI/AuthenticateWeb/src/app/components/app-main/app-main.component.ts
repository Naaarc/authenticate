import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { User } from '../../models/User/User';
import { AuthenticationService } from '../../app-auth/authentication.service';

@Component({
    selector: 'app-app-main',
    templateUrl: './app-main.component.html',
    styleUrls: ['./app-main.component.css']
})
export class AppMainComponent implements OnInit {

    constructor(private readonly auth: AuthenticationService, private readonly router: Router) { }
    user: User;
    ngOnInit() {
        this.user = JSON.parse(localStorage.getItem('currentUser'));
        console.log(this.user);
    }

    logout(): void {
        this.auth.logout();
        this.router.navigate(['/login']);
    }
}
