import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/userService';
import { FormGroup, FormBuilder } from '@angular/forms';
import { User } from '../../models/User/User';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    registerUserModel: User;
    constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }

    ngOnInit() {
        this.registerUserModel = new User();
    }

    private async CreateUser() {
        await this.userService.createUser(this.registerUserModel);
    }

    onSubmit() {
        this.CreateUser()
        this.router.navigateByUrl('/login');
    }
}
