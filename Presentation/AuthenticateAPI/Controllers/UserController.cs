﻿using AuthenticateAPI.Models.Models;
using AuthenticateAPI.Service.Interfaces;
using AuthenticateAPI.ViewModels.VMUser;
using AuthenticateAPI.ViewModels.VMUser.Request;
using AuthenticateAPI.ViewModels.VMUser.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;


namespace AuthenticateAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Route("GetUsers")]
        [HttpGet]
        public BaseResponse<VMGetUserListResponse> GetUsers(VMGetUserListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetUserListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMUserList> result = _userService.GetAll(vmRequest);
                VMGetUserListResponse responseObject = VMUserList.ToResponse(result);
                BaseResponse<VMGetUserListResponse> response = BaseResponse<VMGetUserListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetUserListResponse>.SetError(ex);
            }
        }

        [Authorize(Roles = "Administrator")]
        [Route("GetUser")]
        [HttpGet]
        public BaseResponse<VMGetUserResponse> GetUser(VMGetDeleteUserRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetUserResponse>.SetError("Request body is empty");
            }
            try
            {
                VMUserDetails result = _userService.Get(vmRequest);
                VMGetUserResponse responseObject = VMUserList.ToResponse(result);
                BaseResponse<VMGetUserResponse> response = BaseResponse<VMGetUserResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetUserResponse>.SetError(exc);
            }
        }

        [Route("CreateUser")]
        [HttpPost]
        public BaseResponse<VMGetUserResponse> Create([FromBody]VMCreateUserRequest vmRequset)
        {
            if (vmRequset == null)
            {
                return BaseResponse<VMGetUserResponse>.SetError("request body is empty");
            }
            try
            {
                VMUserDetails result = _userService.Create(vmRequset);
                VMGetUserResponse responseObject = VMUserList.ToResponse(result);
                BaseResponse<VMGetUserResponse> response = BaseResponse<VMGetUserResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetUserResponse>.SetError(exc);
            }
        }

        
        [Route("UpdateUser")]
        [HttpPut]
        public BaseResponse<VMGetUserResponse> Update([FromBody]VMUpdateUserRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetUserResponse>.SetError("Request body is empty");
            }
            try
            {
                VMUserDetails result = _userService.UpdateAndAssignRole(vmRequest);
                VMGetUserResponse responseObject = VMUserList.ToResponse(result);
                BaseResponse<VMGetUserResponse> response = BaseResponse<VMGetUserResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetUserResponse>.SetError(exc);
            }
        }

        [Authorize(Roles = "Administrator")]
        [Route("RemoveUser")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMGetDeleteUserRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("request body is empty");
            }
            try
            {
                bool result = _userService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }
    }
}
