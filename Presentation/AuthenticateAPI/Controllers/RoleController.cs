﻿using AuthenticateAPI.Service.Interfaces;
using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMRole.Request;
using AuthenticateAPI.ViewModels.VMRole.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticateAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Role")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [Route("GetRoles")]
        [HttpGet]
        public BaseResponse<VMGetRoleListResponse> GetUsers(VMGetRoleListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetRoleListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMRoleDetails> result = _roleService.GetAll(vmRequest);
                VMGetRoleListResponse responseObject = VMRoleDetails.ToResponse(result);
                BaseResponse<VMGetRoleListResponse> response = BaseResponse<VMGetRoleListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetRoleListResponse>.SetError(ex);
            }
        }
    }
}
