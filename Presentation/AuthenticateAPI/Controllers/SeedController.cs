﻿using AuthenticateAPI.Service.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenticateAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Seed")]
    public class SeedController : ControllerBase
    {
        private readonly ISeedService _seerdService;

        public SeedController(ISeedService seedService)
        {
            _seerdService = seedService;
        }
        [HttpGet]
        [Route("DoSeed")]
        public BaseResponse<BooleanResponse> DoSeed()
        {
            try
            {
                bool result = _seerdService.Seed();
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<BooleanResponse>.SetError(ex);
            }
        }

    }
}
