﻿using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMUser;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUserRole
{
    [DataContract]
    public class VMUserRoleDetails
    {
        [DataMember]
        public VMUserList User { get; set; }

        [DataMember]
        public VMRoleDetails Role { get; set; }
    }
}
