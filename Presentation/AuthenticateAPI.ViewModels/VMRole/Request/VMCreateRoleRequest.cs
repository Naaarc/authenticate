﻿using AuthenticateAPI.ViewModels.VMUser;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMRole.Request
{
    [DataContract]
    public class VMCreateRoleRequest
    {
        [DataMember]
        public string Name { get; set; }

    }
}
