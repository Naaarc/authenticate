﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMRole.Request
{
    [DataContract]
    public class VMUpdateRoleReuqest : VMCreateRoleRequest
    {
        [DataMember]
        public int RoleId { get; set; }
    }
}
