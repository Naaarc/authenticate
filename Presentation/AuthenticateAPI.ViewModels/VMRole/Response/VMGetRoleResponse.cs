﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMRole.Response
{
    [DataContract]
    public class VMGetRoleResponse
    {
        [DataMember]
        public VMRoleDetails Role { get; set; }
    }
}
