﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMRole.Response
{
    [DataContract]
    public class VMGetRoleListResponse
    {
        [DataMember]
        public List<VMRoleDetails> Roles { get; set; }
    }
}
