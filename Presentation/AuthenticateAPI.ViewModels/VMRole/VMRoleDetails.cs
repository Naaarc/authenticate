﻿using AuthenticateAPI.ViewModels.VMRole.Response;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMRole
{
    [DataContract]
    public class VMRoleDetails
    {
        [DataMember]
        public int RoleId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsAssigned { get; set; }

        public static VMGetRoleListResponse ToResponse(List<VMRoleDetails> vmModel)
        {
            var vmResponse = new VMGetRoleListResponse
            {
                Roles = vmModel
            };

            return vmResponse;
        }
    }
}
