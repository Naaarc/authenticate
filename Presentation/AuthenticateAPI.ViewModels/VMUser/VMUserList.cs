﻿using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMUser.Response;
using AuthenticateAPI.ViewModels.VMUserRole;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUser
{
    [DataContract]
    public class VMUserList
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public VMRoleDetails userRole { get; set; }

        public static VMGetUserListResponse ToResponse(List<VMUserList> vmbsic)
        {
            var vmResponse = new VMGetUserListResponse
            {
                Users = vmbsic
            };
            return vmResponse;
        }

        public static VMGetUserResponse ToResponse(VMUserDetails vmbsic)
        {
            var vmResponse = new VMGetUserResponse
            {
                User = vmbsic
            };
            return vmResponse;
        }
    }
}
