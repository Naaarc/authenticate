﻿using AuthenticateAPI.ViewModels.VMRole;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUser.Request
{
    [DataContract]
    public class VMUpdateUserRequest : VMCreateUserRequest
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public VMRoleDetails UserRole { get; set; }
    }
}
