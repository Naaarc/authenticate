﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUser.Request
{
    [DataContract]
    public class VMGetDeleteUserRequest
    {
        public int UserId { get; set; }
    }
}
