﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUser
{
    [DataContract]
    public class VMUserDetails : VMUserList
    {

        [DataMember]
        public byte[] Password { get; set; }

        [DataMember]
        public byte[] PasswordSalt { get; set; }

        [DataMember]
        public string Token { get; set; }
    }
}
