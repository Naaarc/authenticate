﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace AuthenticateAPI.ViewModels.VMUser.Response
{
    [DataContract]
    public class VMGetUserResponse
    {
        [DataMember]
        public VMUserDetails User { get; set; }
    }
}
