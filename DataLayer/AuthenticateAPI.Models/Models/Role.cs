﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Models.Models
{
    public class Role
    {
        public int RoleID { get; set; }

        public string Name { get; set; }

        //foreign key
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
