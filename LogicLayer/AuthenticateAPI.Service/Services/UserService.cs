﻿using AuthenticateAPI.Database;
using AuthenticateAPI.Models.Models;
using AuthenticateAPI.Service.Helpers;
using AuthenticateAPI.Service.Interfaces;
using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMUser;
using AuthenticateAPI.ViewModels.VMUser.Request;
using AuthenticateAPI.ViewModels.VMUser.Response;
using AuthenticateAPI.ViewModels.VMUserRole;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace AuthenticateAPI.Service.Services
{
    public class UserService : IUserService
    {
        private readonly AuthenticateContext _context;
        private readonly AppSettings _appSettings;
        private readonly IRoleService _roleService;
        public UserService(AuthenticateContext context, IOptions<AppSettings> appSettings, IRoleService roleService)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _roleService = roleService;
        }

        public List<VMUserList> GetAll(VMGetUserListRequest vmRequest)
        {
            List<VMUserList> users = _context.Users
                .AsNoTracking()
                .Select(user => new VMUserList
                {
                    UserId = user.UserID,
                    Name = user.Name,
                    LastName = user.LastName,
                    Email = user.Email,
                    userRole = user.UserRoles.Select(x => new VMRoleDetails
                    {
                        RoleId = x.RoleId,
                        Name = x.Role.Name,
                        IsAssigned = false
                    }).First()

                }).ToList();

            return users;
        }

        public VMUserDetails Get(VMGetDeleteUserRequest vmRequest)
        {
            VMUserDetails user = _context.Users
                .Where(user => user.UserID == vmRequest.UserId)
                .AsNoTracking()
                .Select(user => new VMUserDetails
                {
                    UserId = user.UserID,
                    Name = user.Name,
                    LastName = user.LastName,
                    Email = user.Email,
                    userRole = user.UserRoles.Select(x => new VMRoleDetails
                    {
                        RoleId = x.RoleId,
                        Name = x.Role.Name,
                        IsAssigned = false
                    }).First()

                }).First();

            if (user == null)
            {
                throw new Exception("User could not be found");
            }
            else
            { 
                return user;
            }
        }

        public VMUserDetails Create(VMCreateUserRequest vmRequest)
        {

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(vmRequest.Password, out passwordHash, out passwordSalt);

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    User newUser = new User
                    {
                        Name = vmRequest.Name,
                        LastName = vmRequest.LastName,
                        Email = vmRequest.Email,
                        Password = passwordHash,
                        PasswordSalt = passwordSalt
                    };

                    _context.Users.Add(newUser);
                    _context.SaveChanges();

                    VMUserDetails user = new VMUserDetails
                    {
                        UserId = newUser.UserID,
                        Name = newUser.Name,
                        LastName = newUser.LastName,
                        Email = newUser.Email,
                        Password = newUser.Password
                    };

                    transaction.Commit();
                    return user;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

        public bool Remove(VMGetDeleteUserRequest vmRequest)
        {
            User user = _context.Users
                .Where(user => user.UserID == vmRequest.UserId)
                .First();

            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public VMUserDetails UpdateAndAssignRole(VMUpdateUserRequest vmRequest)
        {
            if (vmRequest == null)
            {
                throw new Exception("User could not be found");
            }

            User userToAssign = _context.Users
                .Where(user => user.UserID == vmRequest.UserId)
                .First();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    userToAssign.Name = vmRequest.Name;
                    userToAssign.LastName = vmRequest.LastName;

                    _roleService.AssignRoleToUser(userToAssign.UserID, vmRequest.UserRole.RoleId);
                    _context.SaveChanges();

                    VMUserDetails user = new VMUserDetails
                    {
                        UserId = userToAssign.UserID,
                        Name = userToAssign.Name,
                        LastName = userToAssign.LastName,
                        Email = userToAssign.Email,
                    };

                    transaction.Commit();
                    return user;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public VMUserDetails Authenticate(string email, string password)
        {
            List<VMUserDetails> users = _context.Users
                .AsNoTracking()
                .Select(user => new VMUserDetails
                {
                    UserId = user.UserID,
                    Name = user.Name,
                    Email = user.Email,
                    Password = user.Password,
                    PasswordSalt = user.PasswordSalt,
                    userRole = user.UserRoles.Select(x => new VMRoleDetails
                    { 
                        Name = x.Role.Name
                    }).First()

                }).ToList();

            VMUserDetails user = users.SingleOrDefault(user => user.Email == email);

            // return null if user not found
            if (user == null || user.userRole.Name == null)
                return null;

            if (!VerifyPassword(password, user.Password, user.PasswordSalt))
                return null;

            // authentication successful so generate jwt token
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString()),
                    new Claim(ClaimTypes.Role, user.userRole.Name)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);
            user.Token = tokenString;
            return user;
        }
        private bool VerifyPassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                // Create hash using password salt.
                byte[] computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password)); 
                for (int i = 0; i < computedHash.Length; i++)
                { 
                    if (computedHash[i] != passwordHash[i]) return false;
                }
            }
            return true;
        }
    }
}
