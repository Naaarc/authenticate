﻿using AuthenticateAPI.Database;
using AuthenticateAPI.Models.Models;
using AuthenticateAPI.Service.Interfaces;
using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMRole.Request;
using AuthenticateAPI.ViewModels.VMRole.Response;
using AuthenticateAPI.ViewModels.VMUserRole;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuthenticateAPI.Service.Services
{
    public class RoleService : IRoleService
    {
        private readonly AuthenticateContext _context;
        public RoleService(AuthenticateContext context)
        {
            _context = context;
        }
        public List<VMRoleDetails> GetAll(VMGetRoleListRequest vmRequest)
        {
            List<VMRoleDetails> roles = _context.Roles
                .AsNoTracking()
                .Select(role => new VMRoleDetails
                {
                    RoleId = role.RoleID,
                    Name = role.Name,
                    IsAssigned = false

                }).ToList();

            return roles;
        }
        public bool AssignRoleToUser(int userId, int roleId)
        {
            UserRole toClear = _context.UserRoles.Where(x => x.UserId == userId).FirstOrDefault();
            if (toClear != null)
            {
                _context.UserRoles.Remove(toClear);
            }
            UserRole userRole = new UserRole
            {
                UserId = userId,
                RoleId = roleId

            };
            _context.UserRoles.Add(userRole);
            _context.SaveChanges();
            return true;
        }
    }
}
