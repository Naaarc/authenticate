﻿using AuthenticateAPI.Database;
using AuthenticateAPI.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Service.Services
{
    public class SeedService : ISeedService
    {
        private readonly AuthenticateContext _context;
        public SeedService(AuthenticateContext context)
        {
            _context = context;
        }

        public bool Seed()
        {
            try
            {
                DataSeed.DoSeed(_context);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
