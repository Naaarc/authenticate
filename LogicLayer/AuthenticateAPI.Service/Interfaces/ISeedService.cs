﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Service.Interfaces
{
    public interface ISeedService
    {
        bool Seed();
    }
}
