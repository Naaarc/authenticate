﻿using AuthenticateAPI.ViewModels.VMRole;
using AuthenticateAPI.ViewModels.VMRole.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Service.Interfaces
{
    public interface IRoleService
    {
        List<VMRoleDetails> GetAll(VMGetRoleListRequest vmRequest);
        bool AssignRoleToUser(int userId, int roleId);
    }
}
