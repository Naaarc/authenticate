﻿using AuthenticateAPI.ViewModels.VMUser;
using AuthenticateAPI.ViewModels.VMUser.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Service.Interfaces
{
    public interface IUserService
    {
        List<VMUserList> GetAll(VMGetUserListRequest vmRequest);
        VMUserDetails Authenticate(string email, string password);
        VMUserDetails Create(VMCreateUserRequest vmRequest);
        VMUserDetails Get(VMGetDeleteUserRequest vmRequest);
        bool Remove(VMGetDeleteUserRequest vmRequest);
        VMUserDetails UpdateAndAssignRole(VMUpdateUserRequest vmRequest);

    }
}
