﻿using AuthenticateAPI.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthenticateAPI.Database
{
    public class AuthenticateContext : DbContext
    {
        public AuthenticateContext(DbContextOptions<AuthenticateContext> options) : base(options)
        { }

        //DbSets
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Role>().ToTable("Role");

            modelBuilder.Entity<UserRole>().ToTable("UserRole")
                .HasKey(x => new { x.UserId, x.RoleId });
        }
    }
}
