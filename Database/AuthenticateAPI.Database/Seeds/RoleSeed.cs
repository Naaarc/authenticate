﻿using AuthenticateAPI.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Database.Seeds
{
    public class RoleSeed
    {
        public static void DoSeed(AuthenticateContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    Role[] roles = new Role[]
                    {
                        new Role{ RoleID = 1, Name = "Administrator"},
                        new Role{ RoleID = 2, Name = "User"}
                    };

                    foreach (Role role in roles)
                    {
                        context.Roles.Add(role);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
