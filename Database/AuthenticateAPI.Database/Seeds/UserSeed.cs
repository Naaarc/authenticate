﻿using AuthenticateAPI.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Database.Seeds
{
    public class UserSeed
    {
        public static void DoSeed(AuthenticateContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    User[] users = new User[]
                    {
                        //new User { UserID = 9050, Name = "Grzegorz", LastName = "Romanow", Email = "grz@mail.com", Password = "password"},
                        //new User { UserID = 9060, Name = "Zbyszek", LastName = "Kieliszek", Email = "kielich@mail.com", Password = "password"},
                        //new User { UserID = 9070, Name = "Robert", LastName = "Mielniczek", Email = "mielnik@mail.com", Password = "password"},
                        //new User { UserID = 9080, Name = "Staszek", LastName = "Ptaszek", Email = "ptak@mail.com", Password = "password"},
                    };

                    foreach (User user in users)
                    {
                        context.Users.Add(user);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

    }
}
