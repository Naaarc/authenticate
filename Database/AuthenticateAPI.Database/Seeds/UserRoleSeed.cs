﻿using AuthenticateAPI.Models.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Database.Seeds
{
    public class UserRoleSeed
    {
        public static void DoSeed(AuthenticateContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    UserRole[] userRoles = new UserRole[]
                    {
                        new UserRole { UserId = 1, RoleId = 1},
                        new UserRole { UserId = 2, RoleId = 2},
                        new UserRole { UserId = 3, RoleId = 2},
                        new UserRole { UserId = 4, RoleId = 2},
                    };

                    foreach (UserRole userRole in userRoles)
                    {
                        context.UserRoles.Add(userRole);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    throw exc;
                }
            }
        }
    }
}
