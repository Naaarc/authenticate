﻿using AuthenticateAPI.Database.Seeds;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthenticateAPI.Database
{
    public static class DataSeed
    {
        public static void DoSeed(AuthenticateContext context)
        {
            RoleSeed.DoSeed(context);
            UserSeed.DoSeed(context);
            UserRoleSeed.DoSeed(context);
        }
    }
}
